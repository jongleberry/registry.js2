
var path = require('path')
var env = process.env

exports.store = path.resolve('cache/normalize-registry') + '/'

// HTTP caching
exports.cacheControl = {
  registry: 'public, max-age=' + (parseInt(process.env.NORMALIZE_MAXAGE_REGISTRY, 10)
    || 60 * 60 * 24 * 365), // 1 year
  versions: 'public, max-age=' + (parseInt(process.env.NORMALIZE_MAXAGE_VERSIONS, 10)
    || 60 * 60), // 1 hour
  semver: 'public, max-age=' + (parseInt(process.env.NORMALIZE_MAXAGE_SEMVER, 10)
    || 60 * 60), // 1 hour
  file: 'public, max-age=' + (parseInt(process.env.NORMALIZE_MAXAGE_FILE, 10)
    || 60 * 60 * 24 * 365), // 1 year
}

// proxy any relevant auths
if (env.NORMALIZE_AUTH_GITHUB) env.GLITTER_GITHUB = env.NORMALIZE_AUTH_GITHUB
if (env.NORMALIZE_AUTH_BITBUCKET) env.GLITTER_BITBUCKET = env.NORMALIZE_AUTH_BITBUCKET
