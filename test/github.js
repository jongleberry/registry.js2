
describe('GitHub Dependencies', function () {
  download('juliangruber/builtins@0.0.5', 'builtins.json')
  download('component/emitter@27f3d7cd66', 'index.js')
  download('component/emitter@27f3d7cd66e1170cba54e6715bfe0e0c148843f2', 'index.js', 200)
})

function download(repo, file, status) {
  var frags = repo.split('@')
  repo = frags[0]
  var version = frags[1]
  var url = '/github/' + repo + '/' + version + '/' + (file || 'index.js')

  it(url, function (done) {
    request.get(url)
    .expect(status || 303, function (err, res) {
      assert.ifError(err)
      if (semver.validRange(version)) fs.statSync(store + url)
      done()
    })
  })
}
