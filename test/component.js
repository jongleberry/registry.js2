
var parse = require('parse-link-header')

describe('Components', function () {
  download('yyx990803/vue@0.10.6')
  download('ripplejs/interpolate@0.4.5')
  download('visionmedia/debug@1.0.1')
  download('visionmedia/mocha@1.18.2', 'mocha.js')
  download('component/classes@1.2.1')
  download('component-test/normalize-multiple-component@0.0.0')
  download('jonathanong/horizontal-grid-packing@0.1.4')
  download('visionmedia/jade@1.3.1')
  download('component/to-function@2.0.0')
  download('ianstormtaylor/to-camel-case@0.2.1')
  download('component/within-document@0.0.1')
  download('component/css@0.0.5')
  download('component/each@0.2.2')
  download('component/s3@0.4.0')
  download('visionmedia/superagent@0.17.0')
  download('jonathanong/eevee@0.0.4')
  download('jonathanong/delegated-dropdown@0.0.7')
  download('jonathanong/autocomplete@0.1.5')
})

function download(repo, file, status) {
  var frags = repo.split('@')
  repo = frags[0]
  var version = frags[1]
  var url = '/github/' + repo + '/' + version + '/' + (file || 'index.js')

  it(url, function (done) {
    request.get(url)
    .expect(status || 303, function (err, res) {
      assert.ifError(err)
      assert(res.headers.link)
      assert(~parse(res.headers.link).version.url.indexOf(version))
      if (semver.validRange(version)) fs.statSync(store + url)
      done()
    })
  })
}
