
var server = require('../app/server').listen()
request = require('supertest').agent(server)

semver = require('semver')
assert = require('assert')
fs = require('fs')

store = require('../config').store

before(function () {
  require('rimraf').sync(require('path').resolve('cache'))
})

require('./api')

require('./github')
require('./npm')

require('./component')
require('./package')
