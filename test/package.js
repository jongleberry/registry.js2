
describe('Packages', function () {
  download('raynos/xtend@3.0.0', 'index.js')
  download('main-loop@1.8.0')
  download('performance-now@0.1.3')
  download('mercury@4.3.0')
  download('WebReflection/dom4@1.0.1')
  download('segmentio/builtins@0.0.4')
  download('twbs/bootstrap@3.1.1', 'index.css')
  download('isaacs/inherits@2.0.1')
  download('defunctzombie/synthetic-dom-events@0.2.2')
  download('url@0.10.1')
  download('util@0.10.3')
})

function download(repo, file, status) {
  var frags = repo.split('@')
  repo = frags[0]
  var version = frags[1]
  frags = repo.split('/')
  var url = ''
  if (frags.length === 1) {
    url = '/npm/-/' + frags[0] + '/' + version + '/' + (file || 'index.js')
    status = 200
  } else {
    url = '/github/' + repo + '/' + version + '/' + (file || 'index.js')
  }

  it(url, function (done) {
    request.get(url)
    .expect(status || 303, function (err, res) {
      assert.ifError(err)
      if (semver.validRange(version)) fs.statSync(store + url)
      done()
    })
  })
}
