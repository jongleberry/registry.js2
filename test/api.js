
describe('GET /registry', function () {
  var registry

  it('should return 200', function (done) {
    request.get('/registry')
    .expect(200)
    .end(function (err, res) {
      assert.ifError(err)
      assert(registry = res.body)
      assert.equal(registry.hostname, 'nlz.io')
      done()
    })
  })

  it('should support github', function () {
    var github = registry.remotes.github
    assert.equal('github', github.name)
    assert.equal('github.com', github.hostname)
    assert(~github.aliases.indexOf('raw.github.com'))
    assert(github.namespace)
  })

  it('should support npm', function () {
    var npm = registry.remotes.npm
    assert.equal('npm', npm.name)
    assert(!npm.namespace)
  })
})

describe('GET /:remote/:user/:project/:version/pull', function () {
  it('should pull the project', function (done) {
    request.get('/github/the-swerve/linear-partitioning/=0.3.1/pull')
    .expect(204, function (err) {
      assert.ifError(err);

      fs.statSync(store + 'github/the-swerve/linear-partitioning/0.3.1/index.js')
      done()
    })
  })

  it('should pull not-installed packages', function (done) {
    request.get('/github/jonathanong/horizontal-grid-packing/pull')
    .expect(204, done)
  })
})

describe('GET /:remote/:user/:project/pull', function () {
  it('should pull the project', function (done) {
    request.get('/github/the-swerve/linear-partitioning/pull')
    .expect(204, function (err, res) {
      assert.ifError(err)
      fs.statSync(store + 'github/the-swerve/linear-partitioning/0.3.1/index.js')
      done()
    })
  })

  it('should get latest npm version', function (done) {
    request.get('/npm/-/domify/pull')
    .expect(204, function (err, res) {
      assert.ifError(err)
      var dirs = fs.readdirSync(store + 'npm/-/domify')
      assert.equal(1, dirs.length)
      done()
    })
  })
})

describe('GET /:remote/:user/:project/versions', function () {
  it('should 404 when there are no versions installed', function (done) {
    request.get('/github/asdfasdf/asdfasdf/versions')
    .expect(404)
    .expect('Content-Type', /application\/json/)
    .end(function (err, res) {
      assert.ifError(err)
      assert.deepEqual(res.body, [])
      done()
    })
  })

  it('should GET github/component-test/index', function (done) {
    request.get('/github/component-test/index/versions')
    .expect(200, function (err, res) {
      assert.ifError(err)
      var versions = res.body.map(toVersion)
      assert(~versions.indexOf('0.0.0'))
      done()
    })
  })

  it('should 404 when no npm packages are installed', function (done) {
    request.get('/npm/-/emitter/versions')
    .expect(404, done)
  })

  it('should pull if ?pull', function (done) {
    request.get('/github/jonathanong/ferver/versions?pull')
    .expect(200, done)
  })
})

describe('Semantic Versions', function () {
  it('should support ^', function (done) {
    request.get('/github/component/emitter/^1.0.0/index.js')
    .expect(303, done)
  })

  it('should support ~', function (done) {
    request.get('/github/component/emitter/~1.0.0/index.js')
    .expect(303, done)
  })

  it('should support <', function (done) {
    request.get('/github/component/emitter/<1/index.js')
    .expect(303, done)
  })

  it('should support >', function (done) {
    request.get('/github/component/emitter/>1.0.0/index.js')
    .expect(303, done)
  })
})


describe('GET nonexistent file', function () {
  it('should 404', function (done) {
    request.get('/github/component-test/deps-any/1f7ca3e47e7525203ca7a0f2e67af2f669d76219/kljljasdfsdafsdf.js')
    .expect(404, done)
  })
})

function toVersion(x) {
  return x[0]
}
