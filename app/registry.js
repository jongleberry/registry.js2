
var json = require('normalize-remotes').json

var cacheControl = require('../config').cacheControl.registry

module.exports = function* (next) {
  if (this.request.path !== '/registry') return yield* next

  this.response.set('Cache-Control', cacheControl)
  if (this.request.fresh) return this.response.status = 304
  this.response.body = json
}
