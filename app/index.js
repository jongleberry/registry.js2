
require('../config') // make sure it's loaded first

var app = module.exports = require('koa')()

app.use(require('koa-cdn')())
app.use(require('koa-favicon')())
if (app.env !== 'production' && app.env !== 'test') app.use(require('koa-logger')())
app.use(require('koa-compressor')())
app.use(require('koa-json-error')())
app.use(require('koa-conditional-get')())
app.use(require('koa-etag')())

app.use(require('./registry'))
app.use(require('./versions'))
app.use(require('./pull'))
app.use(require('./file'))

app.context.uri = require('../lib/uri')
app.context.link = require('../lib/link')
app.context.resolve = require('../lib/resolve')
app.context.remotes = require('normalize-remotes')

app.on('error', function (err) {
  if (err.status >= 500) console.error(err.stack)
})
