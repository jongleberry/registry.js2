
var http = require('http')

var app = require('./')

module.exports = http.createServer(app.callback())

/* istanbul ignore next */
if (!module.parent) {
  module.exports.listen(process.env.PORT || 8888, function (err) {
    if (err) throw err
    console.log('normalize registry listening on port ' + this.address().port)
  })
}
