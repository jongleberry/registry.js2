
var debug = require('debug')('normalize-registry:app:versions')

var cacheControl = require('../config').cacheControl.versions
var Glitter = require('../lib/glitter')
var utils = require('../lib/utils')
var route = require('./route')

var match = route(route.project + '/versions')
var versionsOf = utils.versionsOf

/**
 * Return a list of all currently installed versions of a repository.
 */

module.exports = function* (next) {
  var params = match(this.request.path)
  if (!params) return yield* next

  // pull from source
  var pull = 'pull' in this.request.query

  var res = this.uri.parseRemote(this.request.path)
  var remote = res[0]
  if (!remote.git) {
    var versions = yield* versionsOf(this.uri.local(res.slice(0, 3))) // only gets the latest version
    return respond.call(this, versions.map(toArray))
  }

  var glitter = Glitter(remote.name, res[1], res[2])
  try {
    yield glitter.install()
    if (pull) yield glitter.update()
  } catch (err) {
    debug('%o', err.stack)
    return respond.call(this, [])
  }
  var versions = yield glitter.getVersions()
  return respond.call(this, versions)
}

function respond(versions) {
  this.response.body = versions
  this.response.set('Cache-Control', cacheControl)
  if (!versions.length) this.response.status = 404
}

// keep it consistent with git
function toArray(x) {
  return [x]
}
