
var Glitter = require('../lib/glitter')
var route = require('./route')

var match = route(route.project + '/:version?/pull')

module.exports = function* (next) {
  var path = this.request.path
  var params = match(path)
  if (!params) return yield* next

  var uri = this.uri.parseRemote(path)
  if (!('version' in params)) {
    var remote = uri[0]
    if (remote.git) {
      // git pull the entire repo
      var glitter = Glitter(remote.name, uri[1], uri[2])
      yield glitter.install()
      yield glitter.update()
    } else {
      // get the latest version
      var versions = yield* remote.versions(uri[2])
      uri[3] = versions[0]
      yield* this.resolve(uri)
      this.response.status = 204
    }
    return this.response.status = 204
  }

  yield* this.resolve(uri)
  this.response.status = 204
}
