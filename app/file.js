
var debug = require('debug')('normalize-registry:app:file')
var send = require('koa-sendfile')

var route = require('./route')
var cacheControl = require('../config').cacheControl

// should be able to combine this somehow...
var match = route(route.project + '/:version/:file*')

module.exports = function* (next) {
  var path = this.request.path
  var params = match(path)
  if (!params) return yield* next

  debug('path %s got params %o', path, params)

  var uri = [
    this.remotes(params.remote),
    params.user,
    params.project,
    params.version,
    (params.file || '') || 'index.html'
  ]

  // file exists
  if (yield send.call(this, this.uri.local(uri))) {
    this.response.set('Cache-Control', cacheControl.file)
    return
  }

  uri = yield this.resolve(uri)
  debug('resolved to uri %s', uri)

  var url = this.uri.remote(uri)
  if (url !== path) {
    // semver or some other type of redirect
    // we do this to keep URLs canonical
    this.response.status = 303
    this.response.redirect(url)
    if (uri.version) this.response.set('Link', this.link(uri))
    this.response.set('Cache-Control', cacheControl.semver)
    return
  }

  // send the file
  yield send.call(this, this.uri.local(uri))
  this.response.set('Cache-Control', cacheControl.file)
}
