
var debug = require('debug')('normalize-registry:resolve:resolve')

module.exports = resolve

// any resolves currently in progress

function resolve(uri) {
  debug('resolving %o', uri)

  var remote = uri[0]
  uri[3] = decodeURIComponent(uri[3]) // decode the semver range
  if (remote.git) return resolve.git(uri)
  return resolve.npm(uri)
}
