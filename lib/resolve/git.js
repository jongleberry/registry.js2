
/**
 * Store all the glitter instances.
 * We don't want separate instances because it'll cause race conditions.
 * Need to figure out if this would cause memory-leak issues.
 * TODO: make this a LRU-cache
 */

var debug = require('debug')('normalize-registry:resolve:git')
var assert = require('http-assert')
var tmppath = require('temp-path')
var link = require('fs-symlink')
var semver = require('semver')

var Glitter = require('../glitter')
var install = require('../install')
var uris = require('../uri')

module.exports = _resolve

function* _resolve(uri) {
  var remote = uri[0]
  var owner = uri[1]
  var project = uri[2]
  var range = uri[3]
  var tail = uri[4]
  var glitter = Glitter(remote.name, owner, project)

  // always make sure the repo is installed locally
  yield glitter.install()

  // lookup the reference. if it does not exist locally,
  // git fetch -f and look it up again
  debug('looking up %s', range)
  var reference = yield* lookupReference(glitter, range)
  if (!reference && (yield glitter.update()))
    reference = yield* lookupReference(glitter, range)
    debug('resolved %s to %o', range, reference)

  assert(reference, 404, 'no reference found')
  var sha = reference[1]
  var version = reference.length > 2 ? reference[2] : null

  // output folder
  var tmp = tmppath()
  var out = uris.local(remote, owner, project, sha)
  yield glitter.copy(sha, tmp)
  var newinstall = yield install(tmp, out)
  if (newinstall && version) yield link(out, uris.local(remote, owner, project, version))
  var out = [remote, owner, project, sha, tail]
  out.reference = reference[0]
  if (version) out.version = version
  return out
}

function* lookupReference(glitter, reference) {
  // lookup the local version
  if (semver.validRange(reference)) {
    debug('looking up semver: %s', reference)
    var _reference = yield glitter.getMaxSatisfying(reference)
    debug('got: %o', _reference)
    if (_reference) return _reference.concat(semver.clean(_reference[0]))
  }

  // lookup the local reference
  // if (reference === '*') reference = 'master'
  try {
    var sha = yield glitter.show(reference)
    return [reference, sha]
  } catch (_) {}
}
