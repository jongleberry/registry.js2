
var debug = require('debug')('normalize-registry:resolve:npm')
var assert = require('http-assert')
var tmppath = require('temp-path')
var semver = require('semver')

// fucking circular dependency!
module.exports = _resolve

var install = require('../install')
var utils = require('../utils')
var uris = require('../uri')

var versionsOf = utils.versionsOf

/**
 * Resolve a <hostname>/<user/<project>/<version>/ URL.
 *
 * @param {String} uri
 * @return {Object} file
 * @api private
 */

function* _resolve(uri) {
  var remote = uri[0]
  var owner = uri[1]
  var project = uri[2]
  var range = uri[3]
  var tail = uri[4]
  var remoteURI = uris.remote(uri)
  assert(semver.validRange(range), 400, 'Invalid semantic version: ' + remoteURI)

  var out = uris.local(remote, owner, project, '')
  var versions = yield* versionsOf(out)
  var version = semver.maxSatisfying(versions, range)
  var out
  if (version) {
    out += version
    // if the folder exists, but the repository is currently being installed,
    // then we wait for it to install
    if (install.installing[out]) {
      debug('waiting to be installed: %s', out)
      yield install.installing[out]
    }
  } else {
    // no satisfying verisons, so we install a new one
    var repo = owner + '/' + project
    debug('resolving %s/%s@%s', remote.hostname, repo, range)
    // not installed,
    // so we install the latest that satisfies this range
    // To do: make sure someone can't DDOS us by spamming invalid versions.
    // To do: do state checking so we don't unnecessarily use HTTP requests
    versions = yield* remote.versions(repo, range)
    assert(versions.length, 404, 'No versions found for: ' + remoteURI)
    version = semver.maxSatisfying(versions, range)
    assert(version, 404, 'No satisfying versions found: ' + remoteURI)
    out += semver.clean(version)
    var tmp = tmppath()
    yield* remote.download(repo, version, tmp)
    yield install(tmp, out)
    version = semver.clean(version)
  }

  return [remote, owner, project, version, tail]
}
