
module.exports = require('./resolve')

/**
 * To make normalization run faster, we add additional rewrite processes.
 * Note that 1 child processes is already created by default
 * and the current process already uses 1 CPU.
 * We also keep an extra CPU open for extra background processes.
 */

var forks = require('os').cpus().length - 3
var rewrite = require('rewrite-js-dependencies/master')
for (var i = 0; i < forks; i++) rewrite.fork()
