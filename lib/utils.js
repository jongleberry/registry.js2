
var semver = require('semver')
var fs = require('mz/fs')

/**
 * Return the versions installed at a `path`,
 * where the path looks like:
 *
 *   ../<hostname/<user>/<repo>
 *
 * Since versions are stored as nested directories
 *
 * @param {String} path
 * @return {Array} versions
 * @api public
 */

exports.versionsOf = function* (path) {
  return (yield fs.readdir(path).catch(returnEmptyDir))
    .filter(validVersion)
    .sort(semver.rcompare)
}

function returnEmptyDir() {
  return []
}

function validVersion(x) {
  if (x[0] === '.') return false
  return semver.valid(x)
}
