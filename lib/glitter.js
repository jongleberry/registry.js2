
var Glitter = require('glitter')
var lru = require('lru-cache')

// hopefully avoids any potential memory leaks
// but we don't want an infinite amount of instances of glitters
var glitters = lru({
  max: 1000
})

module.exports = function (remote, user, project) {
  var key = remote + ':' + user + '/' + project
  var glitter = glitters.get(key)
  if (glitter) return glitter
  var glitter = Glitter(remote, user, project)
  glitters.set(key, glitter)
  return glitter
}
