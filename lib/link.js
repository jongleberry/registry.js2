
/**
 * Create a `link` header for the client
 * so they know whether the version and reference.
 * There could be easier ways of sending this data,
 * but I don't feel like creating my own header fields.
 * Suggestions welcomed.
 */

var uris = require('./uri')

module.exports = function (resolved) {
  // no reference
  if (!resolved.reference) return

  var str = '<'
  var _res = resolved.slice()
  _res[3] = resolved.reference
  str += uris.remote(_res) + '>; rel="reference"'

  if (resolved.version) {
    _res = resolved.slice()
    _res[3] = resolved.version
    str += ',<' + uris.remote(_res) + '>; rel="version"'
  }

  return str
}
