
/**
 * Install to a temporary file, then normalize, then move to the final folder.
 */

var normalize = require('normalize-package')
var exec = require('mz/child_process').exec
var dirname = require('path').dirname
var co = require('co')

module.exports = install

var installing = install.installing = Object.create(null)

function* install(temp, out) {
  if (installing[out]) return yield installing[out].then(function () {
    return false
  })
  return installing[out] = co(_install(temp, out)).then(function () {
    delete installing[out]
    return true
  }, function (err) {
    delete installing[out]
    throw err
  })
}

function* _install(temp, out) {
  yield normalize(temp)
  yield exec('mkdir -p ' + dirname(out) + '; mv ' + temp + ' ' + out)
}
