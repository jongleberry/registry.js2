
# registry.js

[![NPM version][npm-image]][npm-url]
[![Build status][travis-image]][travis-url]
[![Test coverage][coveralls-image]][coveralls-url]
[![Dependency Status][david-image]][david-url]
[![License][license-image]][license-url]
[![Downloads][downloads-image]][downloads-url]
[![Gittip][gittip-image]][gittip-url]

This is the registry for [normalize](http://normalize.github.io),
which is hosted at http://nlz.io.
It basically:

- Resolves versions and git shas
- Grabs files from the sources
- Normalizes them to `http://nlz.io/` URLs via [normalize-package](https://github.com/normalize/package.js)
- Serves the normalized files over a CDN

The idea behind this registry is to normalize all dependencies into URLs,
allowing any client to resolve these dependencies, including browsers!.

This registry has been rewritten a couple times, only to be simpler.
Previously, it used SPDY push to serve files quickly.
However, this was not easy to deploy and costs a lot of money due to the lack of a CDN.
Above all, it made the codebase much more complicated.

## Usage

The primary use-case for this registry is for normalize.
If you use normalize without private repositories,
you don't have to know anything about this registry.

### Private Repositories

You can create a registry with private repositories easily by adding authentication details. See [config.js](config.js) for details.

### Custom Remotes

You can add custom remotes by adding custom remotes to [normalize-remotes](https://github.com/normalize/remotes.js)
during runtime or forking it.

## API

### GET /registry

Get the current information for the registry.
This is useful for custom registries which may use authentication for their remotes
(i.e. use private repositories) as well as adding custom remotes
(i.e. GitHub enterprise or something).

### GET /:remote/:user/:package/:version/:file*

Get a file. If the version must be resolved, it will return a redirect.

Remotes are:

- `npm`
- `github`
- `bitbucket`

User is the username for that repository.
For global `npm` packages, this `user` should just be `-`.

The version is any semantic version.
For git remotes, any reference is acceptable,
but will __always__ redirect to the git commit sha.

Example:

```js
GET /github/component/emitter/^1.0.0/index.js
GET /npm/-/mocha/2/mocha.js
GET /npm/-/mocha/2/mocha.css
```

### GET /:remote/:user/:package/versions

Get all semantic versions for a package.

### GET /:remote/:user/:package/:version?/pull

Pull the latest or a specific version to the registry.
This is the "publish" step.
Use this if you publish a new version of a module and want to use it immediately.

[gitter-image]: https://badges.gitter.im/normalize/registry.js.png
[gitter-url]: https://gitter.im/normalize/registry.js
[npm-image]: https://img.shields.io/npm/v/normalize-registry.svg?style=flat-square
[npm-url]: https://npmjs.org/package/normalize-registry
[github-tag]: http://img.shields.io/github/tag/normalize/registry.js.svg?style=flat-square
[github-url]: https://github.com/normalize/registry.js/tags
[travis-image]: https://img.shields.io/travis/normalize/registry.js.svg?style=flat-square
[travis-url]: https://travis-ci.org/normalize/registry.js
[coveralls-image]: https://img.shields.io/coveralls/normalize/registry.js.svg?style=flat-square
[coveralls-url]: https://coveralls.io/r/normalize/registry.js
[david-image]: http://img.shields.io/david/normalize/registry.js.svg?style=flat-square
[david-url]: https://david-dm.org/normalize/registry.js
[license-image]: http://img.shields.io/npm/l/normalize-registry.svg?style=flat-square
[license-url]: LICENSE
[downloads-image]: http://img.shields.io/npm/dm/normalize-registry.svg?style=flat-square
[downloads-url]: https://npmjs.org/package/normalize-registry
[gittip-image]: https://img.shields.io/gratipay/jonathanong.svg?style=flat-square
[gittip-url]: https://gratipay.com/jonathanong/
